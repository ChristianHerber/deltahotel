<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hotel Delta - O Melhor da Cidade</title>
	<meta charset="utf-8">
	<meta name="description" content="Um dos melhores hotéis da cidade de Diamantino é o Hotel Delta, localizado no centro da cidade na Avenida Desembargador Joaquim Pereira Ferreira Mendes. Oferece aos seus hospedes conforto e segurança em excelentes acomodações.">
	<meta name="keywords" content="hotel, apartamentos, quartos, diamantino, delta, hotel delta, hotel delta diamantino, confortavel, arejado">
	<meta name="author" content="Uebi! Agência Digital">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Bree+Serif" rel="stylesheet">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/fontawesome.min.css">
	<link rel="icon" href="<?php bloginfo('template_url'); ?>/img/favicon.png">
	<?php wp_head(); ?>
</head>
<body itemscope itemtype="http://schema.org/Hotel">