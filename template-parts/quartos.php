	<div class="container-fluid" id="quartos">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h2 class="bree">Apartamentos</h2>
				</div>

			<?php
			    global $postAptos;
			    $argsAptos = array ( 'post_type'=>'apartamento', 'post_per_page'=>3 );
			    $mypostsAptos = get_posts ($argsAptos);
			    foreach ( $mypostsAptos as $postAptos ): setup_postdata($postAptos);
			    $image_idAptos = get_post_thumbnail_id($postAptos->ID);
			    $image_urlAptos = wp_get_attachment_image_src($image_idAptos, 'full');
			    $imageAptosDestaque = $image_urlAptos[0];
			    $tituloAptos = $postAptos->post_title;
			    $contentAptos = $postAptos->post_content;
			    $linkAptos = get_permalink($postAptos->ID);
			?>

				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12" itemprop="containsPlace" itemscope itemtype="http://schema.org/HotelRoom">
					<a href="<?php echo $linkAptos; ?>/#categoria">
						<img src="<?php echo $imageAptosDestaque; ?>" alt="<?php echo $tituloAptos; ?>" class="img-fluid">
						<h2 class="bree title" itemprop="name"><?php echo $tituloAptos; ?></h2>
						<p><span itemprop="description">
							<?php echo substr($contentAptos, 0, 150); ?>...
						</span></p>
					</a>
					<a href="<?php echo $linkAptos; ?>/#categoria" class="btn btn-success">Mais detalhes</a>
				</div>

				<?php endforeach; ?>

			</div>
		</div>
	</div>