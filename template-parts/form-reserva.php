	<div class="container-fluid" id="reserva">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="bree">Garanta sua reserva!</h2>
					<form id="frm-reserva" class="frm-reserva" name="frm-reserva" method="post">
					  <div class="form-row align-items-center">

					    <div class="col-auto frm-input">
					      <label class="sr-only" for="dataChegada">Nome</label>
					      <div class="input-group mb-2">
					        <div class="input-group-prepend">
					          <div class="input-group-text"><i class="fa fa-address-card-o"></i></div>
					        </div>
					        <input type="text" name="nome" class="form-control" id="nome" placeholder="Nome" required="required">
					      </div>
					    </div>

					    <div class="col-auto frm-input">
					      <label class="sr-only" for="dataChegada">E-mail</label>
					      <div class="input-group mb-2">
					        <div class="input-group-prepend">
					          <div class="input-group-text"><i class="fa fa-envelope"></i></div>
					        </div>
					        <input type="email" name="email" class="form-control" id="email" placeholder="E-mail" required="required">
					      </div>
					    </div>

					    <div class="col-auto frm-input">
					      <label class="sr-only" for="dataChegada">Telefone</label>
					      <div class="input-group mb-2">
					        <div class="input-group-prepend">
					          <div class="input-group-text"><i class="fa fa-phone"></i></div>
					        </div>
					        <input type="text" name="telefone" class="form-control" id="telefone" placeholder="Telefone" required="required">
					      </div>
					    </div>

					    <div class="col-auto frm-input">
					      <label class="sr-only" for="dataChegada">Data de Chegada</label>
					      <div class="input-group mb-2">
					        <div class="input-group-prepend">
					          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
					        </div>
					        <?php $date = date('Y-m-d'); ?>
					        <input type="date" name="data-chegada" class="form-control" id="dataChegada" value="<?php echo $date; ?>" required="required">
					      </div>
					    </div>

					    <input type="hidden" name="assunto" value="Reserva">

					    <div class="col-auto frm-input">
					      <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-check-square-o fa-lg"></i></button>
					    </div>
					  </div>
					</form>
					<?php
				    	global $postInfoTop;
					    $argsInfoTop = array ( 'post_type'=>'dado-do-hotel', 'post_per_page'=>1 );
					    $mypostsInfoTop = get_posts ($argsInfoTop);
					    foreach ( $mypostsInfoTop as $postInfoTop ): setup_postdata($postInfoTop);
					    $customInfoTop = get_post_custom($postInfoTop->ID);
			    	?>
					<p><i class="fa fa-map-marker fa-lg"></i> <?php echo $customInfoTop['wpcf-endereco'][0]; ?> <i class="fa fa-phone fa-lg"></i> <?php echo $customInfoTop['wpcf-telefone'][0]; ?></p>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>