	<div class="container-fluid text-center" id="info" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
		<div class="container">
			<div class="row">
				<?php
				    global $postInfo;
				    $argsInfo = array ( 'post_type'=>'dado-do-hotel', 'post_per_page'=>1 );
				    $mypostsInfo = get_posts ($argsInfo);
				    foreach ( $mypostsInfo as $postInfo ): setup_postdata($postInfo);
				    $customInfo = get_post_custom($postInfo->ID);
			    ?>

				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
					<p itemprop="telephone"><i class="fa fa-phone fa-lg"></i> <?php echo $customInfo['wpcf-telefone'][0]; ?></p>
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
					<p itemprop="streetAddress"><i class="fa fa-map-marker fa-lg"></i> <?php echo $customInfo['wpcf-endereco'][0]; ?></p>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
					<p itemprop="email"><i class="fa fa-envelope fa-lg"></i> <?php echo $customInfo['wpcf-e-mail'][0]; ?></p>
				</div>

			<?php endforeach; ?>

			</div>
		</div>
	</div>