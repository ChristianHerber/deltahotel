	<div class="container-fluid" id="contato">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h2 class="bree">Contato</h2>
				</div>
			</div>
			<form id="frm-reserva" class="frm-reserva" name="frm-reserva" method="post">
			  <div class="form-row">
			    <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
			      <input type="text" name="nome" class="form-control form-control-lg" id="name" placeholder="Nome">
			    </div>
			    <div class="form-group col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
			      <input type="email" name="email" class="form-control form-control-lg" id="email" placeholder="E-mail" required="required">
			    </div>
			    <div class="form-group col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
			      <input type="text" name="telefone" class="form-control form-control-lg" id="telefone" placeholder="Telefone">
			    </div>
			  </div>
			  <div class="form-row">
			    <div class="form-group col-12">
			      <textarea name="msg" class="form-control form-control-lg" id="msg" placeholder="Como posso lhe ajudar?" required="required"></textarea>
			    </div>
			  </div>
			  <div class="form-row">
			    <div class="form-group col-12">
			  	  <button type="submit" class="form-control btn btn-lg btn-success"><i class="fa fa-send fa-lg"></i> Enviar</button>
			    </div>
			  </div>
			  <input type="hidden" name="assunto" value="Contato">
			</form>
		</div>
	</div>