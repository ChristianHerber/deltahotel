	<div class="container-fluid" id="sobre">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h2 class="bree" itemprop="name">Hotel Delta</h2>
				</div>
				<div class="col-12 text-justify" itemprop="description">
				    <?php
				        $pageId = 7;
				        $id = get_post($pageId);

				        echo wpautop($id->post_content);
				    ?>
				</div>
			</div>
		</div>
	</div>