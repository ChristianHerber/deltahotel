	<div class="container-fluid" id="categoria">
		<div class="container">
			<div class="row">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div class="col-12 text-center">
					<h2 class="bree"><?php the_title(); ?></h2>
				</div>

				<div class="col-12">
					<?php the_content(); ?>
				</div>

				<?php endwhile; ?>
				<?php else: ?>
				<?php endif; ?>

			</div>
		</div>
	</div>