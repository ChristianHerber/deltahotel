	<div class="container-fluid menu">
		<div class="">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 logo">
					<a href="/"><img itemprop="logo" src="<?php bloginfo('template_url'); ?>/img/logo-delta.png" alt="Hotel Delta - O Melhor da Cidade" class="img-fluid"></a>
				</div>
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
					<nav class="navbar navbar-expand-lg">
						<div class="container">
							<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"><i class="fa fa-bars fa-lg fa-2x"></i></span>
							</button>

							<div class="collapse navbar-collapse" id="navbarNav">
								<ul class="navbar-nav mr-auto navigation">
								  <li class="nav-item">
								    <a class="nav-link" href="<?php if (is_front_page()){home_url();} ?>/#sobre">Sobre</a>
								  </li>
								  <li class="nav-item">
								    <a class="nav-link" href="<?php if (is_front_page()){home_url();} ?>/#quartos">Apartamentos</a>
								  </li>
								  <li class="nav-item">
								    <a class="nav-link" href="<?php if (is_front_page()){home_url();} ?>/#imgs">Fotos</a>
								  </li>
								  <li class="nav-item">
								    <a class="nav-link" href="<?php if (is_front_page()){home_url();} ?>/#caracteristicas">Serviços</a>
								  </li>
								  <li class="nav-item">
								    <a class="nav-link" href="<?php if (is_front_page()){home_url();} ?>/#contato">Contato</a>
								  </li>
								  <!-- <li class="nav-item">
								    <a class="nav-link" href="#"><i class="fa fa-facebook fa-lg"></i></a>
								  </li> -->
								</ul>
							</div>
						</div>
					</nav>
				</div>
			</div>
		</div>
		<div class="selo">
			<div id="TA_rated841" class="TA_rated">
				<ul id="py8AmaH" class="TA_links ZmaKHt9Ix">
					<li id="WEdVJckU76" class="3bI7j1Y9z2">
						<a target="_blank" href="https://www.tripadvisor.com.br/"><img src="https://www.tripadvisor.com.br/img/cdsi/img2/badges/ollie-11424-2.gif" alt="TripAdvisor"/></a>
					</li>
				</ul>
			</div>
			<script src="https://www.jscache.com/wejs?wtype=rated&amp;uniq=841&amp;locationId=12908138&amp;lang=pt&amp;display_version=2"></script>
		</div>
	</div>

	<div class="topo-mobile"></div>