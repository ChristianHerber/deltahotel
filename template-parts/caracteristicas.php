	<div class="container-fluid text-center" id="caracteristicas" itemprop="amenityFeature" itemscope itemtype="http://schema.org/LocationFeatureSpecification">
		<div class="row">
			<div class="col-xl-2 offset-md-1 col-md-2" data-toggle="tooltip" title="Apartamentos com Ar-condicionado">
				<p><i class="fa fa-snowflake-o fa-lg fa-3x"></i></p>
				<p class="bree" itemprop="name">Ar-condicionado</p>
			</div>
			<div class="col-xl-2 col-md-2" data-toggle="tooltip" title="Wi-fi gratuito">
				<p><i class="fa fa-wifi fa-lg fa-3x"></i></p>
				<p class="bree" itemprop="name">Wi-fi</p>
			</div>
			<div class="col-xl-2 col-md-2" data-toggle="tooltip" title="Café da manhã gratuito">
				<p><i class="fa fa-coffee fa-lg fa-3x"></i></p>
				<p class="bree" itemprop="name">Café da Manhã</p>
			</div>
			<div class="col-xl-2 col-md-2" data-toggle="tooltip" title="Estacionamento amplo e coberto">
				<p><i class="fa fa-car fa-lg fa-3x"></i></p>
				<p class="bree" itemprop="name">Estacionamento</p>
			</div>
			<div class="col-xl-2 col-md-2" data-toggle="tooltip" title="Serviço de lavanderia">
				<p><i class="fa fa-tint fa-lg fa-3x"></i></p>
				<p class="bree" itemprop="name">Lavanderia</p>
			</div>
		</div>
	</div>