<?php
	register_nav_menu('menu_topo', 'delta');
	add_post_type_support( 'page', 'excerpt' );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'image-slider', 1600, 703, TRUE );