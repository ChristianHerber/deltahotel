</body>
	<script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/popper.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script>
	<?php wp_footer(); ?>
	<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "Organization",
	  "url": "http://www.deltahotel.com.br",
	  "logo": "http://www.deltahotel.com.br/img/logo-delta.png",
	  "contactPoint": [{
	    "@type": "ContactPoint",
	    "telephone": "+55-65-33326-1778",
	    "contactType": "customer service"
	  }]
	}
	</script>
</html>