<?php
 if (preg_match('/deltahotel.com.br/', $_SERVER[HTTP_HOST])) {
         $emailsender='DELTAHOTEL@deltahotel.com.br'; 
 } else {
         $emailsender = "DELTAHOTEL@" . $_SERVER[HTTP_HOST];
 }

 if(PATH_SEPARATOR == ";") $quebra_linha = "\r\n"; 
	else $quebra_linha = "\n";
 
// Passando os dados obtidos pelo formulário para as variáveis abaixo

$nomeremetente     = utf8_decode($_POST['nome']);
$emailremetente    = $_POST['email'];
$telefone          = utf8_decode($_POST['telefone']);
$emaildestinatario = 'christianfaldox@gmail.com';
$assunto           = utf8_decode($_POST['assunto']);

if($assunto == 'Reserva'){
  $d = $_POST['data-chegada'];
  $mensagem        = '<strong>Data de Chegada:</strong> '.date('d/m/Y', strtotime($d));
} else {
  $mensagem        = utf8_decode($_POST['msg']);
}

/**
* Montando a mensagem, como ele será visualizada
**/
$mensagemHTML = 
'
<table style="margin: auto; min-width: 800px;">
  <tr>
    <th style="font-weight:bold;font-size:16px;background-color:#005131;color:#ffffff;padding:15px 10px;" colspan="2">Você recebeu uma mensagem de contato através do site www.deltahotel.com.br</th>
  </tr>
  <tr>
    <td style="vertical-align:top;">Remetente:</td>
    <td style="vertical-align:top;font-weight: bold;">'.$nomeremetente.'</td>
  </tr>
  <tr>
    <td style="vertical-align:top;">E-mail:</td>
    <td style="vertical-align:top;font-weight: bold;">'.$emailremetente.'</td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Telefone:</td>
    <td style="vertical-align:top;font-weight: bold;">'.$telefone.'</td>
  </tr>
  <tr>
    <td style="vertical-align:top;">Assunto:</td>
    <td style="vertical-align:top;font-weight: bold;">'.$assunto.'</td>
  </tr>
  <tr>
    <td style="vertical-align:top;border-top: 1px solid #ccc;padding-top: 5px;" id="msg" colspan="2">'.$mensagem.'</td>
  </tr>
</table>
'.$quebra_linha;



$headers = "MIME-Version: 1.1" .$quebra_linha;
$headers .= "Content-type: text/html; charset=iso-8859-1" .$quebra_linha;
$headers .= "From: " . $emailsender.$quebra_linha;
$headers .= "Reply-To: " . $emailremetente . $quebra_linha;

if(!mail($emaildestinatario, $assunto, $mensagemHTML, $headers ,"-r".$emailsender)){
    $headers .= "Return-Path: " . $emailsender . $quebra_linha;
    mail($emaildestinatario, $assunto, $mensagemHTML, $headers );
	//wp_mail( $emaildestinatario, $assunto, $mensagemHTML, $headers);
}
 
?>